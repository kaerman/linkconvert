package com.kaerman.link.converter.controller;

import com.kaerman.link.converter.domain.document.LinkConvertMessage;
import com.kaerman.link.converter.dto.DataResponse;
import com.kaerman.link.converter.service.LinkService;
import com.kaerman.link.converter.dto.LinkConvertRequest;
import com.kaerman.link.converter.dto.LinkConvertResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Date;
import java.util.Objects;

@Slf4j
@RestController
@RequestMapping("/link")
@RequiredArgsConstructor
public class LinkController {

    private final LinkService linkService;

    @Operation(summary = "Convert weblink to deeplink vice-versa")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Converted link",
                    content = { @Content(mediaType = "application/json", schema = @Schema(implementation = LinkConvertResponse.class))})
    })
    @PostMapping (value = "/convert", produces = "application/json")
    public DataResponse<LinkConvertResponse> convertLink(@Valid @RequestBody LinkConvertRequest linkConvertRequest){
        validate(linkConvertRequest);
        String convertedUrl = linkService.convertLink(linkConvertRequest);
        LinkConvertResponse linkConvertResponse = LinkConvertResponse.builder()
                .url(convertedUrl)
                .build();
        LinkConvertMessage linkConvertMessage = saveConvertLinkRequestAndResponseMessage(linkConvertRequest, linkConvertResponse);
        return DataResponse.of(linkConvertMessage.getId(), linkConvertResponse);
    }

    private void validate(LinkConvertRequest linkConvertRequest) {
        log.info("Convert request {}", linkConvertRequest);
        if (!StringUtils.hasText(linkConvertRequest.getUrl())
                || Objects.isNull(linkConvertRequest.getDestinationLinkType()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "LinkConvertRequest is not valid, message " + linkConvertRequest);
    }

    private LinkConvertMessage saveConvertLinkRequestAndResponseMessage(LinkConvertRequest linkConvertRequest, LinkConvertResponse linkConvertResponse) {
        LinkConvertMessage linkConvertMessage = LinkConvertMessage.builder()
                .requestMessage(linkConvertRequest)
                .responseMessage(linkConvertResponse)
                .timestamp(new Date())
                .build();
        linkConvertMessage = linkService.saveLinkConvertMessage(linkConvertMessage);
        return linkConvertMessage;
    }
}
