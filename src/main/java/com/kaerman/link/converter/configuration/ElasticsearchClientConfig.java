package com.kaerman.link.converter.configuration;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackages = "com.kaerman.link.converter.repository")
@ComponentScan(basePackages = { "com.kaerman.link.converter.domain.document" })
public class ElasticsearchClientConfig extends AbstractElasticsearchConfiguration {

  @Value("${elasticSearch.hostAndPort:localhost:9200}")
  private String elasticSearchHostAndPort;

  @Bean
  public RestHighLevelClient elasticsearchClient() {
    final ClientConfiguration clientConfiguration = ClientConfiguration.builder()
            .connectedTo(elasticSearchHostAndPort)
            .build();
    return RestClients.create(clientConfiguration).rest();
  }
}