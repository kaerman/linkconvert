package com.kaerman.link.converter.repository;

import com.kaerman.link.converter.domain.document.LinkConvertMessage;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface LinkRepository extends ElasticsearchRepository<LinkConvertMessage, String> {
}
