package com.kaerman.link.converter.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.kaerman.link.converter.domain.converter.LinkConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LinkConvertResponse {
    private String url;
    private LinkConverter.Type linkType;
}
