package com.kaerman.link.converter.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataResponse<T> {
    private String id;
    private T response;

    public static <R> DataResponse<R> of(String id, R response) {
        return new DataResponse<>(id, response);
    }
}
