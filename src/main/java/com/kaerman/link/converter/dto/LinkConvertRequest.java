package com.kaerman.link.converter.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.kaerman.link.converter.domain.converter.LinkConverter;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LinkConvertRequest {
    @NotBlank
    private String url;
    @NotNull
    private LinkConverter.Type destinationLinkType;
}
