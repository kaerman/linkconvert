package com.kaerman.link.converter.service;

import com.kaerman.link.converter.domain.converter.LinkConverter;
import com.kaerman.link.converter.domain.converter.LinkConverterFactory;
import com.kaerman.link.converter.domain.document.LinkConvertMessage;
import com.kaerman.link.converter.dto.LinkConvertRequest;
import com.kaerman.link.converter.repository.LinkRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LinkService {

    private final LinkConverterFactory linkConverterFactory;
    private final LinkRepository linkRepository;

    public String convertLink(LinkConvertRequest linkConvertRequest) {
        LinkConverter linkConverter = linkConverterFactory.getLinkConverter(linkConvertRequest.getDestinationLinkType());
        return linkConverter.convert(linkConvertRequest.getUrl());
    }

    public LinkConvertMessage saveLinkConvertMessage(LinkConvertMessage linkConvertMessage) {
        return linkRepository.save(linkConvertMessage);
    }
}
