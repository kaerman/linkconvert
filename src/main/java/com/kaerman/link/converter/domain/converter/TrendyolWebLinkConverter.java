package com.kaerman.link.converter.domain.converter;

import com.kaerman.link.converter.domain.*;
import lombok.Builder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import static com.kaerman.link.converter.domain.TrendyolPage.WEB_LINK_SCHEME_SECURE;

@Component
@Builder
public class TrendyolWebLinkConverter implements LinkConverter {

    public static final String BOUTIQUE_ID = "boutiqueId=";
    public static final String MERCHANT_ID = "merchantId=";

    @Override
    public String convert(String linkUrl) {
        Link link = Link.parse(linkUrl);
        Link.LinkBuilder linkBuilder = Link.builder().scheme(WEB_LINK_SCHEME_SECURE)
                .host(TrendyolMainPage.TRENDYOL_WEB_MAIN_PAGE);
        TrendyolPage trendyolPage = TrendyolPageFactory.createTrendyolPage(link);

        String query;
        switch (trendyolPage.getTrendyolPageType()) {
            case PRODUCT_PAGE:
                String path = mapProductWebLinkPath((TrendyolProductPage) trendyolPage);
                linkBuilder.path(path);
                query = mapProductWebLinkQuery((TrendyolProductPage) trendyolPage);
                if (StringUtils.hasText(query)) {
                    linkBuilder.query(query);
                }
                break;
            case SEARCH_PAGE:
                query = mapSearchWebLink((TrendyolSearchPage) trendyolPage);
                linkBuilder.path("sr");
                linkBuilder.query(query);
                break;
            case MAIN_PAGE:
            default:
        }
        return linkBuilder.build().toString();
    }

    private String mapSearchWebLink(TrendyolSearchPage trendyolPage) {
        return "q=" + trendyolPage.getQuery();
    }

    private String mapProductWebLinkPath(TrendyolProductPage trendyolPage) {
        return "brand/name-p-" + trendyolPage.getContentId();
    }

    private String mapProductWebLinkQuery(TrendyolProductPage trendyolPage) {
        StringBuilder queryBuilder = new StringBuilder();
        if (StringUtils.hasText(trendyolPage.getBoutiqueId())) {
            queryBuilder.append(BOUTIQUE_ID).append(trendyolPage.getBoutiqueId());
        }
        if (StringUtils.hasText(trendyolPage.getMerchantId())) {
            if (queryBuilder.length() > 0)
                queryBuilder.append("&");
            queryBuilder.append(MERCHANT_ID).append(trendyolPage.getMerchantId());
        }
        return queryBuilder.toString();
    }

    @Override
    public Type getType() {
        return Type.WEB_LINK;
    }
}
