package com.kaerman.link.converter.domain.converter;

public interface LinkConverter {

    enum Type {
        WEB_LINK, DEEP_LINK
    }

    String convert(String link);

    Type getType();
}
