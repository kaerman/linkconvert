package com.kaerman.link.converter.domain;

import java.util.HashMap;
import java.util.Map;

import static com.kaerman.link.converter.domain.TrendyolPage.*;

public class TrendyolPageFactory {

    public static final Map<TrendyolPageType, TrendyolPage> trendyolPageMap = new HashMap<>();
    static {
        trendyolPageMap.put(TrendyolPageType.MAIN_PAGE, TrendyolMainPage.builder().build());
        trendyolPageMap.put(TrendyolPageType.PRODUCT_PAGE, TrendyolProductPage.builder().build());
        trendyolPageMap.put(TrendyolPageType.SEARCH_PAGE, TrendyolSearchPage.builder().build());
    }

    public static TrendyolPage createTrendyolPage(Link link) {
        TrendyolPage.validate(link);
        for (TrendyolPageType trendyolPageType : trendyolPageMap.keySet()) {
            TrendyolPage trendyolPage = trendyolPageMap.get(trendyolPageType);
            if (trendyolPage.linkMatcher(link)) {
                return trendyolPage.createPage(link);
            }
        }
        return trendyolPageMap.get(TrendyolPageType.MAIN_PAGE).createPage(link);
    }
}
