package com.kaerman.link.converter.domain.converter;

import com.kaerman.link.converter.domain.*;
import lombok.Builder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
@Builder
public class TrendyolDeepLinkConverter implements LinkConverter {

    public static final String PAGE_PRODUCT_CONTENT_ID = "Page=Product&ContentId=";
    public static final String CAMPAIGN_ID = "&CampaignId=";
    public static final String MERCHANT_ID = "&MerchantId=";
    public static final String PAGE_SEARCH_QUERY = "Page=Search&Query=";

    @Override
    public String convert(String linkUrl) {
        Link link = Link.parse(linkUrl);
        Link.LinkBuilder linkBuilder = Link.builder().scheme(TrendyolPage.DEEP_LINK_SCHEME);
        TrendyolPage trendyolPage = TrendyolPageFactory.createTrendyolPage(link);

        String query;
        switch (trendyolPage.getTrendyolPageType()) {
            case PRODUCT_PAGE:
                query = mapProductDeepLink((TrendyolProductPage) trendyolPage);
                break;
            case SEARCH_PAGE:
                query = mapSearchDeepLink((TrendyolSearchPage) trendyolPage);
                break;
            case MAIN_PAGE:
            default:
                query = TrendyolMainPage.TRENDYOL_DEEPLINK_MAIN_PAGE_QUERY;
        }
        linkBuilder.query(query);
        return linkBuilder.build().toString();
    }

    private String mapSearchDeepLink(TrendyolSearchPage trendyolSearchPage) {
        return PAGE_SEARCH_QUERY + trendyolSearchPage.getQuery();
    }

    private String mapProductDeepLink(TrendyolProductPage trendyolProductPage) {
        StringBuilder query = new StringBuilder();
        query.append(PAGE_PRODUCT_CONTENT_ID).append(trendyolProductPage.getContentId());
        if (StringUtils.hasText(trendyolProductPage.getBoutiqueId())) {
            query.append(CAMPAIGN_ID).append(trendyolProductPage.getBoutiqueId());
        }
        if (StringUtils.hasText(trendyolProductPage.getMerchantId())) {
            query.append(MERCHANT_ID).append(trendyolProductPage.getMerchantId());
        }
        return query.toString();
    }

    @Override
    public Type getType() {
        return Type.DEEP_LINK;
    }
}
