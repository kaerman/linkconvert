package com.kaerman.link.converter.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.AbstractMap.*;

@Getter
@Setter
public abstract class TrendyolPage {

    public enum TrendyolPageType {
        MAIN_PAGE, PRODUCT_PAGE, SEARCH_PAGE
    }

    public final static String WEB_LINK_SCHEME_SECURE = "https";
    public final static String WEB_LINK_SCHEME = "http";
    public final static String DEEP_LINK_SCHEME = "ty";
    public static final String TRENDYOL_WEB_MAIN_PAGE = "www.trendyol.com";
    public static final String TRENDYOL_DEEPLINK_MAIN_PAGE_QUERY = "Page=Home";

    private final static SimpleImmutableEntry<String, String> httpsEntry = new SimpleImmutableEntry<>(WEB_LINK_SCHEME_SECURE, TRENDYOL_WEB_MAIN_PAGE);
    private final static SimpleImmutableEntry<String, String> httpEntry = new SimpleImmutableEntry<>(WEB_LINK_SCHEME, TRENDYOL_WEB_MAIN_PAGE);
    private final static SimpleImmutableEntry<String, String> tyEntry = new SimpleImmutableEntry<>(DEEP_LINK_SCHEME, null);

    private final static Map<String, SimpleImmutableEntry<String, String>> validSchemes =
            Stream.of(httpsEntry, httpEntry, tyEntry)
                    .collect(Collectors.toUnmodifiableMap(SimpleImmutableEntry::getKey, entry -> entry));

    public static void validate(Link link) {
        if (!validSchemes.containsKey(link.getScheme())
                || (Objects.nonNull(link.getHost()) && !validSchemes.get(link.getScheme()).getValue().equals(link.getHost()))) {
            throw new IllegalArgumentException(String.format("No valid trendyol link %s://%s", link.getScheme(), link.getHost()));
        }
    }

    protected Link link;

    public abstract TrendyolPageType getTrendyolPageType();

    public abstract boolean linkMatcher(Link link);

    public abstract TrendyolPage createPage(Link link);
}
