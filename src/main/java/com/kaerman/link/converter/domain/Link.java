package com.kaerman.link.converter.domain;

import lombok.Builder;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriUtils;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.nio.charset.StandardCharsets;

@Builder
public class Link {

    public static final String AUTHORITY_START_DELIMITER = "//";
    public static final String SCHEME_DELIMITER = ":" + AUTHORITY_START_DELIMITER;
    public static final String PORT_DELIMITER = ":";
    public static final String PATH_DELIMITER = "/";
    public static final String QUERY_DELIMITER = "?";
    private static final int NOT_EXIST = -1;

    private final String scheme;
    private final String host;
    private final int port;
    private final String path;
    private final String query;

    Link(final String scheme, final String host, final int port, final String path, final String query) {
        this.scheme = StringUtils.hasText(scheme) ? UriUtils.encodeScheme(scheme, StandardCharsets.UTF_8) : null;
        this.host = StringUtils.hasText(host) ? UriUtils.encodeHost(host, StandardCharsets.UTF_8) : null;
        this.port = port;
        this.path = StringUtils.hasText(path) ? UriUtils.encodePath(path, StandardCharsets.UTF_8) : null;
        this.query = StringUtils.hasText(query) ? UriUtils.encodeQuery(query, StandardCharsets.UTF_8) : null;
    }

    public String getScheme() {
        return StringUtils.hasText(scheme) ? UriUtils.decode(scheme, StandardCharsets.UTF_8) : null;
    }

    public String getHost() {
        return StringUtils.hasText(host) ? UriUtils.decode(host, StandardCharsets.UTF_8) : null;
    }

    public int getPort() {
        return port;
    }

    public String getPath() {
        return StringUtils.hasText(path) ? UriUtils.decode(path, StandardCharsets.UTF_8) : null;
    }

    public String getQuery() {
        return StringUtils.hasText(query) ? UriUtils.decode(query, StandardCharsets.UTF_8) : null;
    }

    public static Link parse(@Valid @NotBlank final String link) {
        LinkBuilder linkBuilder = Link.builder();
        int schemeDelimiterIndex = mapScheme(link, linkBuilder);
        int pathDelimiterIndex = mapHostAndPort(link, linkBuilder, schemeDelimiterIndex);
        int queryDelimiterIndex = mapPath(link, linkBuilder, pathDelimiterIndex);
        mapQuery(link, linkBuilder, queryDelimiterIndex);
        return linkBuilder.build();
    }

    private static int mapScheme(String link, LinkBuilder linkBuilder) {
        int schemeSeparatorIndex = link.indexOf(SCHEME_DELIMITER);
        if (schemeSeparatorIndex == NOT_EXIST) {
            throw new IllegalArgumentException(String.format("Link is not valid %s", link));
        }
        linkBuilder.scheme(link.substring(0, schemeSeparatorIndex));
        schemeSeparatorIndex += SCHEME_DELIMITER.length();
        return schemeSeparatorIndex;
    }

    private static int mapHostAndPort(String link, LinkBuilder linkBuilder, int schemeDelimiterIndex) {
        int portSeparatorIndex =  link.indexOf(PORT_DELIMITER, schemeDelimiterIndex);
        int pathDelimiterIndex = link.indexOf(PATH_DELIMITER, schemeDelimiterIndex);
        if (portSeparatorIndex == NOT_EXIST) {
            if (pathDelimiterIndex == NOT_EXIST) {
                int queryDelimiterIndex = link.indexOf(QUERY_DELIMITER, schemeDelimiterIndex);
                if (queryDelimiterIndex == NOT_EXIST) {
                    linkBuilder.host(link.substring(schemeDelimiterIndex));
                } else {
                    return queryDelimiterIndex;
                }
            } else {
                linkBuilder.host(link.substring(schemeDelimiterIndex, pathDelimiterIndex));
                pathDelimiterIndex += PATH_DELIMITER.length();
            }
        } else {
            linkBuilder.host(link.substring(schemeDelimiterIndex, portSeparatorIndex));
            portSeparatorIndex += PORT_DELIMITER.length();
            if (portSeparatorIndex != pathDelimiterIndex) {
                linkBuilder.port(Integer.parseInt(link.substring(portSeparatorIndex, pathDelimiterIndex)));
            }
            pathDelimiterIndex += PATH_DELIMITER.length();
        }

        return pathDelimiterIndex;
    }

    private static int mapPath(String link, LinkBuilder linkBuilder, int pathDelimiterIndex) {
        if (pathDelimiterIndex == NOT_EXIST)
            return NOT_EXIST;
        int queryDelimiterIndex = link.indexOf(QUERY_DELIMITER, pathDelimiterIndex);
        if (queryDelimiterIndex == NOT_EXIST) {
            linkBuilder.path(link.substring(pathDelimiterIndex));
        } else {
            if (pathDelimiterIndex != queryDelimiterIndex) {
                linkBuilder.path(link.substring(pathDelimiterIndex, queryDelimiterIndex));
            }
            queryDelimiterIndex += QUERY_DELIMITER.length();
        }
        return queryDelimiterIndex;
    }

    private static void mapQuery(String link, LinkBuilder linkBuilder, int queryDelimiterIndex) {
        if (queryDelimiterIndex == NOT_EXIST)
            return;
        linkBuilder.query(link.substring(queryDelimiterIndex));
    }

    public String toString() {
        if (!StringUtils.hasText(getScheme())) {
            return null;
        }
        String linkStr = getScheme() + SCHEME_DELIMITER;
        if (StringUtils.hasText(getHost())) {
            linkStr += getHost();
        }
        if (getPort() != 0) {
            linkStr += PORT_DELIMITER + getPort();
        }
        if (StringUtils.hasText(getPath())) {
            linkStr += PATH_DELIMITER + getPath();
        }
        if (StringUtils.hasText(getQuery())) {
            linkStr += QUERY_DELIMITER + getQuery();
        }
        return linkStr;
    }

}
