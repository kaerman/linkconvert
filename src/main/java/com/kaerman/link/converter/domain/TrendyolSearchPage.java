package com.kaerman.link.converter.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriUtils;

import java.nio.charset.StandardCharsets;
import java.util.regex.Pattern;

@Getter
@Setter
@Builder
public class TrendyolSearchPage extends TrendyolPage {

    public static final String SEARCH_WEB_PAGE_PATH = "sr";
    public static final String SEARCH_WEB_PAGE_QUERY = "q=";
    public static final String SEARCH_DEEPLINK_PAGE_QUERY = "Page=Search";
    public static final String TURKISH_CHARACTERS = "çÇğĞıİöÖşŞüÜ";
    public static final String QUERY_PARAM_SEPERATOR = "&";
    public static final String QUERY_EQUALS_SEPERATOR = "=";
    public static final int QUERY_PARAM_KEY_INDEX = 0;
    public static final int QUERY_PARAM_VALUE_INDEX = 1;
    public static final int QUERY_PARAM_VARIABLE_LENGTH = 2;
    public static final String QUERY_PARAM = "Query";
    public static final Pattern pattern = Pattern.compile(".*[" + TURKISH_CHARACTERS +"].*");
    private String query;

    @Override
    public TrendyolPageType getTrendyolPageType() {
        return TrendyolPageType.SEARCH_PAGE;
    }

    @Override
    public boolean linkMatcher(Link link) {
        return webSearchPageMatch(link) || deeplinkSearchPageMatch(link);
    }

    private boolean webSearchPageMatch(Link link) {
        return link.getScheme().startsWith(TrendyolPage.WEB_LINK_SCHEME)
                && StringUtils.hasText(link.getPath())
                && link.getPath().startsWith(SEARCH_WEB_PAGE_PATH);
    }

    private boolean deeplinkSearchPageMatch(Link link) {
        return link.getScheme().startsWith(TrendyolPage.DEEP_LINK_SCHEME)
                && StringUtils.hasText(link.getQuery())
                && link.getQuery().startsWith(SEARCH_DEEPLINK_PAGE_QUERY);
    }


    @Override
    public TrendyolPage createPage(Link link) {
        if (link.getScheme().startsWith(TrendyolPage.WEB_LINK_SCHEME)) {
            return createSearchPageFromWebLink(link);
        } else if (link.getScheme().equals(TrendyolPage.DEEP_LINK_SCHEME)) {
            return createSearchPageFromDeepLink(link);
        }
        throw new IllegalArgumentException("Link is not trendyol weblink or deeplink , link ->" + link);
    }

    private TrendyolSearchPage createSearchPageFromWebLink(Link link) {
        validateWebSearchPageLink(link);
        TrendyolSearchPageBuilder trendyolSearchPageBuilder = TrendyolSearchPage.builder();
        String query = link.getQuery().substring(SEARCH_WEB_PAGE_QUERY.length());
        trendyolSearchPageBuilder.query(encodeQueryIfContainsTurkishCharacter(query));
        return trendyolSearchPageBuilder.build();
    }

    private void validateWebSearchPageLink(Link link) {
        if (!SEARCH_WEB_PAGE_PATH.equals(link.getPath()) || !link.getQuery().startsWith(SEARCH_WEB_PAGE_QUERY)) throw new IllegalArgumentException("Search page link is not valid, link " + link);
    }

    private String encodeQueryIfContainsTurkishCharacter(String query) {
        return StringUtils.hasText(query) && pattern.matcher(query).matches() ? UriUtils.encode(query, StandardCharsets.UTF_8) : query;
    }

    private TrendyolSearchPage createSearchPageFromDeepLink(Link link) {
        TrendyolSearchPageBuilder trendyolSearchPageBuilder = TrendyolSearchPage.builder();
        String[] queryParams = link.getQuery().split(QUERY_PARAM_SEPERATOR);
        for (String param : queryParams) {
            mapQueryParameter(param, trendyolSearchPageBuilder);
        }
        return trendyolSearchPageBuilder.build();
    }

    private void mapQueryParameter(String param, TrendyolSearchPageBuilder trendyolSearchPageBuilder) {
        String[] paramKeyValuePair = param.split(QUERY_EQUALS_SEPERATOR);
        if (paramKeyValuePair.length == QUERY_PARAM_VARIABLE_LENGTH &&
                QUERY_PARAM.equals(paramKeyValuePair[QUERY_PARAM_KEY_INDEX])) {
                String query = paramKeyValuePair[QUERY_PARAM_VALUE_INDEX];
                trendyolSearchPageBuilder.query(encodeQueryIfContainsTurkishCharacter(query));
        }
    }
}
