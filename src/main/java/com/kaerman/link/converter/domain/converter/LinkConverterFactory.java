package com.kaerman.link.converter.domain.converter;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class LinkConverterFactory {

    private final Map<LinkConverter.Type, LinkConverter> linkConverterMap;

    public LinkConverterFactory(Set<LinkConverter> linkConverterSet) {
        this.linkConverterMap = new HashMap<>();
        linkConverterSet.forEach(
                linkConverter -> linkConverterMap.put(linkConverter.getType(), linkConverter));
    }

    public LinkConverter getLinkConverter(LinkConverter.Type type) {
        return linkConverterMap.get(type);
    }
}
