package com.kaerman.link.converter.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

import java.util.stream.Stream;

@Getter
@Setter
@Builder
public class TrendyolProductPage extends TrendyolPage {

    public static final int BRAND_NAME_OR_CATEGORY_NAME_INDEX = 0;
    public static final int PRODUCT_NAME_AND_CONTENT_ID_INDEX = 1;
    public static final int PRODUCT_PATH_VARIABLE_LENGTH = 2;
    public static final int PRODUCT_NAME_INDEX = 0;
    public static final int CONTENT_ID_INDEX = 1;
    public static final String MERCHANT_ID = "merchantId=";
    public static final String BOUTIQUE_ID = "boutiqueId=";
    public static final String PRODUCT_CONTENT_SEPERATOR = "-p-";
    public static final String PATH_SEPERATOR = "/";
    public static final int QUERY_PARAM_VARIABLE_LENGTH = 2;
    public static final int QUERY_PARAM_KEY_INDEX = 0;
    public static final int QUERY_PARAM_VALUE_INDEX = 1;
    public static final String QUERY_PARAM_SEPERATOR = "&";
    public static final String QUERY_EQUALS_SEPERATOR = "=";
    public static final String QUERY_PAGE_PARAM = "Page";
    public static final String QUERY_CONTENT_ID_PARAM = "ContentId";
    public static final String QUERY_CAMPAIGN_ID_PARAM = "CampaignId";
    public static final String QUERY_MERCHANT_ID_PARAM = "MerchantId";

    private String brandName;
    private String categoryName;
    private String productName;
    private String contentId;
    private String boutiqueId;
    private String merchantId;

    @Override
    public TrendyolPageType getTrendyolPageType() {
        return TrendyolPageType.PRODUCT_PAGE;
    }

    @Override
    public boolean linkMatcher(Link link) {
        return webProductPageMatch(link) || deeplinkProductPageMatch(link);
    }

    private boolean webProductPageMatch(Link link) {
        return link.getScheme().startsWith(TrendyolPage.WEB_LINK_SCHEME)
                && StringUtils.hasText(link.getPath())
                && link.getPath().contains(PRODUCT_CONTENT_SEPERATOR);
    }

    private boolean deeplinkProductPageMatch(Link link) {
        return link.getScheme().startsWith(TrendyolPage.DEEP_LINK_SCHEME)
                && StringUtils.hasText(link.getQuery())
                && link.getQuery().startsWith("Page=Product");
    }

    @Override
    public TrendyolPage createPage(Link link) {
        if (link.getScheme().startsWith(TrendyolPage.WEB_LINK_SCHEME)) {
            return createProductPageFromWebLink(link);
        } else if (link.getScheme().equals(TrendyolPage.DEEP_LINK_SCHEME)) {
            return createProductPageFromDeepLink(link);
        }
        throw new IllegalArgumentException("Link is not trendyol weblink or deeplink , link ->" + link);
    }

    private TrendyolPage createProductPageFromWebLink(Link link) {
        String[] pageDetails = link.getPath().split(PATH_SEPERATOR);
        validateProductPageLink(pageDetails);
        TrendyolProductPage.TrendyolProductPageBuilder trendyolProductPageBuilder = TrendyolProductPage.builder();
        String[] productNameAndContentId = pageDetails[PRODUCT_NAME_AND_CONTENT_ID_INDEX].split(PRODUCT_CONTENT_SEPERATOR);
        if (productNameAndContentId.length == PRODUCT_PATH_VARIABLE_LENGTH) {
            trendyolProductPageBuilder.brandName(pageDetails[BRAND_NAME_OR_CATEGORY_NAME_INDEX]);
            trendyolProductPageBuilder.productName(productNameAndContentId[PRODUCT_NAME_INDEX]);
            trendyolProductPageBuilder.contentId(productNameAndContentId[CONTENT_ID_INDEX]);
        }
        if (StringUtils.hasText(link.getQuery())) {
            String[] queryParams = link.getQuery().split(QUERY_PARAM_SEPERATOR);
            trendyolProductPageBuilder.boutiqueId(mapBoutiqueIdFromWebLink(queryParams));
            trendyolProductPageBuilder.merchantId(mapMerchantIdFromWebLink(queryParams));
        }

        return validateProductPage(trendyolProductPageBuilder.build(), link);
    }

    private TrendyolPage validateProductPage(TrendyolProductPage trendyolProductPage, Link link) {
        if (!StringUtils.hasText(trendyolProductPage.getContentId()))
            throw new IllegalArgumentException("Product page link is not valid, link " + link);
        return trendyolProductPage;
    }

    private void validateProductPageLink(String[] pageDetails) {
        if (pageDetails.length != PRODUCT_PATH_VARIABLE_LENGTH) throw new IllegalArgumentException("Product page link is not valid, link " + link);
        if (!pageDetails[PRODUCT_NAME_AND_CONTENT_ID_INDEX].contains(PRODUCT_CONTENT_SEPERATOR)) throw new IllegalArgumentException("Product page link is not valid, link" + link);
    }

    private String mapMerchantIdFromWebLink(String[] queryParams) {
        return Stream.of(queryParams).filter(parameter -> parameter.startsWith(MERCHANT_ID))
                .map(merchantParam -> merchantParam.substring(MERCHANT_ID.length())).findFirst().orElse(null);
    }

    private String mapBoutiqueIdFromWebLink(String[] queryParams) {
        return Stream.of(queryParams).filter(parameter -> parameter.startsWith(BOUTIQUE_ID))
                .map(boutiqueParam -> boutiqueParam.substring(BOUTIQUE_ID.length())).findFirst().orElse(null);
    }

    private TrendyolPage createProductPageFromDeepLink(Link link) {
        TrendyolProductPage.TrendyolProductPageBuilder trendyolProductPageBuilder = TrendyolProductPage.builder();
        String[] queryParams = link.getQuery().split(QUERY_PARAM_SEPERATOR);
        for (String param : queryParams) {
            mapQueryParameter(param, trendyolProductPageBuilder);
        }
        return validateProductPage(trendyolProductPageBuilder.build(), link);
    }

    private void mapQueryParameter(String param, TrendyolProductPageBuilder trendyolProductPageBuilder) {
        String[] paramKeyValuePair = param.split(QUERY_EQUALS_SEPERATOR);
        if (paramKeyValuePair.length == QUERY_PARAM_VARIABLE_LENGTH) {
            switch (paramKeyValuePair[QUERY_PARAM_KEY_INDEX]) {
                case QUERY_PAGE_PARAM:
                    break;
                case QUERY_CONTENT_ID_PARAM:
                    trendyolProductPageBuilder.contentId(paramKeyValuePair[QUERY_PARAM_VALUE_INDEX]);
                    break;
                case QUERY_CAMPAIGN_ID_PARAM:
                    trendyolProductPageBuilder.boutiqueId(paramKeyValuePair[QUERY_PARAM_VALUE_INDEX]);
                    break;
                case QUERY_MERCHANT_ID_PARAM:
                    trendyolProductPageBuilder.merchantId(paramKeyValuePair[QUERY_PARAM_VALUE_INDEX]);
                    break;
            }
        }
    }
}
