package com.kaerman.link.converter.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

@Getter
@Setter
@Builder
public class TrendyolMainPage extends TrendyolPage {

    @Override
    public TrendyolPageType getTrendyolPageType() {
        return TrendyolPageType.MAIN_PAGE;
    }

    @Override
    public boolean linkMatcher(Link link) {
        return webMainPageMatch(link) || deeplinkMainPageMatch(link);
    }

    private boolean deeplinkMainPageMatch(Link link) {
        return link.getScheme().equals(TrendyolPage.DEEP_LINK_SCHEME)
                && StringUtils.hasText(link.getQuery())
                && link.getQuery().equals(TrendyolPage.TRENDYOL_DEEPLINK_MAIN_PAGE_QUERY);
    }

    private boolean webMainPageMatch(Link link) {
        return StringUtils.hasText(link.getHost())
                && !StringUtils.hasText(link.getPath())
                && link.getHost().equals(TrendyolPage.TRENDYOL_WEB_MAIN_PAGE);
    }

    @Override
    public TrendyolPage createPage(Link link) {
        return TrendyolMainPage.builder().build();
    }
}
