package com.kaerman.link.converter.domain.document;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kaerman.link.converter.dto.LinkConvertRequest;
import com.kaerman.link.converter.dto.LinkConvertResponse;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;

@Data
@Builder
@Document(indexName = "linkconvertindex")
public class LinkConvertMessage {

    @Id
    private String id;

    @Field(type = FieldType.Object, name = "request")
    private LinkConvertRequest requestMessage;

    @Field(type = FieldType.Object, name = "response")
    private LinkConvertResponse responseMessage;

    @Field(type = FieldType.Date, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZZ", name="timestamp")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern ="yyyy-MM-dd'T'HH:mm:ss.SSSZZ")
    private Date timestamp;

}