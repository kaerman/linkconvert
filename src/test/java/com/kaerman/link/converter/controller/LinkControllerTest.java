package com.kaerman.link.converter.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kaerman.link.converter.domain.converter.LinkConverter;
import com.kaerman.link.converter.domain.document.LinkConvertMessage;
import com.kaerman.link.converter.dto.DataResponse;
import com.kaerman.link.converter.dto.LinkConvertRequest;
import com.kaerman.link.converter.dto.LinkConvertResponse;
import com.kaerman.link.converter.service.LinkService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Date;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(LinkController.class)
class LinkControllerTest {

    private static final String MY_URL = "https://com.kaerman";
    private static final String WEB_REQUEST_URL = "https://www.trendyol.com/casio/saat-p-1925865?boutiqueId=439892&merchantId=105064";
    private static final String DEEPLINK_RESPONSE_URL = "ty://?Page=Product&ContentId=1925865&CampaignId=439892&MerchantId=105064";
    private static final String WEB_RESPONSE_URL = "https://www.trendyol.com/brand/name-p-1925865?boutiqueId=439892&merchantId=105064";
    private static final String DEEPLINK_REQUEST_URL = "ty://?Page=Product&ContentId=1925865&CampaignId=439892&MerchantId=105064";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LinkService linkService;

    @Captor
    ArgumentCaptor<LinkConvertMessage> linkConvertMessageArgumentCaptor;

    private static ObjectMapper objectMapper;

    @BeforeAll
    static void init() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void it_should_convert_link_when_link_controller_convert_request_with_weblink() throws Exception {
        //given
        LinkConvertRequest linkConvertRequest = LinkConvertRequest.builder()
                .url(WEB_REQUEST_URL)
                .destinationLinkType(LinkConverter.Type.DEEP_LINK)
                .build();
        LinkConvertResponse linkConvertResponse = LinkConvertResponse.builder()
                .url(DEEPLINK_RESPONSE_URL)
                .linkType(LinkConverter.Type.DEEP_LINK)
                .build();
        LinkConvertMessage savedLinkConvertMessage = LinkConvertMessage.builder()
                .id(UUID.randomUUID().toString())
                .requestMessage(linkConvertRequest)
                .responseMessage(linkConvertResponse)
                .timestamp(new Date())
                .build();
        String requestContent = objectMapper.writeValueAsString(linkConvertRequest);
        given(linkService.convertLink(linkConvertRequest)).willReturn(linkConvertResponse.getUrl());
        given(linkService.saveLinkConvertMessage(any(LinkConvertMessage.class))).willReturn(savedLinkConvertMessage);

        //when
        MvcResult mvcResult = mockMvc.perform(post("/link/convert")
                    .content(requestContent)
                    .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.response.url").value(DEEPLINK_RESPONSE_URL))
                .andReturn();
        DataResponse<LinkConvertResponse> linkConvertDataResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), DataResponse.class);

        //then
        then(linkService).should().saveLinkConvertMessage(linkConvertMessageArgumentCaptor.capture());
        assertThat(linkConvertMessageArgumentCaptor.getValue().getRequestMessage()).isNotNull();
        assertThat(linkConvertMessageArgumentCaptor.getValue().getResponseMessage()).isNotNull();
        assertThat(savedLinkConvertMessage.getId()).isEqualTo(linkConvertDataResponse.getId());
    }

    @Test
    void it_should_return_bad_request_when_link_controller_convert_request_url_is_not_valid_with_weblink() throws Exception {
        //given
        LinkConvertRequest linkConvertRequest = LinkConvertRequest.builder()
                .url(MY_URL)
                .destinationLinkType(LinkConverter.Type.DEEP_LINK)
                .build();
        String requestContent = objectMapper.writeValueAsString(linkConvertRequest);
        given(linkService.convertLink(linkConvertRequest)).willThrow(new IllegalArgumentException());

        //when
        MvcResult mvcResult = mockMvc.perform(post("/link/convert")
                .content(requestContent)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        //then
        assertThat(mvcResult.getResponse().getStatus()).isEqualTo(400);
    }

    @Test
    void it_should_convert_link_when_link_controller_convert_request_with_deeplink() throws Exception {
        //given
        LinkConvertRequest linkConvertRequest = LinkConvertRequest.builder()
                .url(DEEPLINK_REQUEST_URL)
                .destinationLinkType(LinkConverter.Type.WEB_LINK)
                .build();
        LinkConvertResponse linkConvertResponse = LinkConvertResponse.builder()
                .url(WEB_RESPONSE_URL)
                .linkType(LinkConverter.Type.WEB_LINK)
                .build();
        LinkConvertMessage savedLinkConvertMessage = LinkConvertMessage.builder()
                .id(UUID.randomUUID().toString())
                .requestMessage(linkConvertRequest)
                .responseMessage(linkConvertResponse)
                .timestamp(new Date())
                .build();
        String requestContent = objectMapper.writeValueAsString(linkConvertRequest);
        given(linkService.convertLink(linkConvertRequest)).willReturn(linkConvertResponse.getUrl());
        given(linkService.saveLinkConvertMessage(any(LinkConvertMessage.class))).willReturn(savedLinkConvertMessage);

        //when
        MvcResult mvcResult = mockMvc.perform(post("/link/convert")
                .content(requestContent)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.response.url").value(WEB_RESPONSE_URL))
                .andReturn();
        DataResponse<LinkConvertResponse> linkConvertDataResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), DataResponse.class);

        //then
        then(linkService).should().saveLinkConvertMessage(linkConvertMessageArgumentCaptor.capture());
        assertThat(linkConvertMessageArgumentCaptor.getValue().getRequestMessage()).isNotNull();
        assertThat(linkConvertMessageArgumentCaptor.getValue().getResponseMessage()).isNotNull();
        assertThat(savedLinkConvertMessage.getId()).isEqualTo(linkConvertDataResponse.getId());
    }

    @Test
    void it_should_return_bad_request_when_link_controller_convert_request_url_is_not_valid() throws Exception {
        //given
        LinkConvertRequest linkConvertRequest = LinkConvertRequest.builder()
                .url(MY_URL)
                .destinationLinkType(LinkConverter.Type.WEB_LINK)
                .build();
        String requestContent = objectMapper.writeValueAsString(linkConvertRequest);
        given(linkService.convertLink(linkConvertRequest)).willThrow(new IllegalArgumentException());

        //when
        MvcResult mvcResult = mockMvc.perform(post("/link/convert")
                .content(requestContent)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        //then
        assertThat(mvcResult.getResponse().getStatus()).isEqualTo(400);
    }
}