package com.kaerman.link.converter.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kaerman.link.converter.domain.converter.LinkConverter;
import com.kaerman.link.converter.domain.document.LinkConvertMessage;
import com.kaerman.link.converter.dto.DataResponse;
import com.kaerman.link.converter.dto.LinkConvertRequest;
import com.kaerman.link.converter.dto.LinkConvertResponse;
import com.kaerman.link.converter.repository.LinkRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.testcontainers.elasticsearch.ElasticsearchContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Testcontainers
@AutoConfigureMockMvc
@ContextConfiguration(initializers = {LinkControllerContainerIntegrationTest.Initializer.class})
@SpringBootTest
public class LinkControllerContainerIntegrationTest {

    private static final String MY_URL = "https://com.kaerman";
    private static final String WEB_REQUEST_NOT_HAVE_CONTENT_ID_URL = "https://www.trendyol.com/casio/saat-p-?boutiqueId=439892&merchantId=105064";
    private static final String DEEPLINK_REQUEST_NOT_HAVE_CONTENT_ID_URL = "ty://?Page=Product&ContentId=&CampaignId=439892&MerchantId=105064";
    private static final String WEB_REQUEST_URL = "https://www.trendyol.com/casio/saat-p-1925865?boutiqueId=439892&merchantId=105064";
    private static final String DEEPLINK_RESPONSE_URL = "ty://?Page=Product&ContentId=1925865&CampaignId=439892&MerchantId=105064";
    private static final String WEB_RESPONSE_URL = "https://www.trendyol.com/brand/name-p-1925865?boutiqueId=439892&merchantId=105064";
    private static final String DEEPLINK_REQUEST_URL = "ty://?Page=Product&ContentId=1925865&CampaignId=439892&MerchantId=105064";


    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "elasticSearch.hostAndPort=" + elasticsearchContainer.getHttpHostAddress()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }

    @Container
    public static ElasticsearchContainer elasticsearchContainer = new ElasticsearchContainer("docker.elastic.co/elasticsearch/elasticsearch:7.12.0")
            .withExposedPorts(9200, 9300);

    private static ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    LinkRepository linkRepository;

    @BeforeAll
    static void init() {
        objectMapper = new ObjectMapper();
    }

    @Test
    public void it_should_save_linkConvertMessage_when_link_service_save_with_weblink() throws Exception {
        //given
        LinkConvertRequest linkConvertRequest = LinkConvertRequest.builder()
                .url(WEB_REQUEST_URL)
                .destinationLinkType(LinkConverter.Type.DEEP_LINK)
                .build();
        String requestContent = objectMapper.writeValueAsString(linkConvertRequest);

        //when
        MvcResult mvcResult = mockMvc.perform(post("/link/convert")
                .content(requestContent)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        DataResponse<LinkConvertResponse> linkConvertDataResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<DataResponse<LinkConvertResponse>>(){});

        //then
        assertThat(linkConvertDataResponse.getResponse().getUrl()).isEqualTo(DEEPLINK_RESPONSE_URL);
        assertThat(linkConvertDataResponse.getId()).isNotBlank();
        Optional<LinkConvertMessage> convertMessage = linkRepository.findById(linkConvertDataResponse.getId());
        assertThat(convertMessage.isPresent()).isTrue();
        assertThat(convertMessage.get().getId()).isEqualTo(linkConvertDataResponse.getId());
    }

    @Test
    public void it_should_save_linkConvertMessage_when_link_service_save_with_deeplink() throws Exception {
        //given
        LinkConvertRequest linkConvertRequest = LinkConvertRequest.builder()
                .url(DEEPLINK_REQUEST_URL)
                .destinationLinkType(LinkConverter.Type.WEB_LINK)
                .build();
        String requestContent = objectMapper.writeValueAsString(linkConvertRequest);

        //when
        MvcResult mvcResult = mockMvc.perform(post("/link/convert")
                .content(requestContent)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        DataResponse<LinkConvertResponse> linkConvertDataResponse = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<DataResponse<LinkConvertResponse>>(){});

        //then
        assertThat(linkConvertDataResponse.getResponse().getUrl()).isEqualTo(WEB_RESPONSE_URL);
        assertThat(linkConvertDataResponse.getId()).isNotBlank();
        Optional<LinkConvertMessage> convertMessage = linkRepository.findById(linkConvertDataResponse.getId());
        assertThat(convertMessage.isPresent()).isTrue();
        assertThat(convertMessage.get().getId()).isEqualTo(linkConvertDataResponse.getId());
    }


    @Test
    void it_should_return_bad_request_when_link_controller_convert_request_url_is_not_valid() throws Exception {
        //given
        LinkConvertRequest linkConvertRequest = LinkConvertRequest.builder()
                .url(MY_URL)
                .destinationLinkType(LinkConverter.Type.DEEP_LINK)
                .build();
        String requestContent = objectMapper.writeValueAsString(linkConvertRequest);

        //when
        MvcResult mvcResult = mockMvc.perform(post("/link/convert")
                .content(requestContent)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        //then
        assertThat(mvcResult.getResponse().getStatus()).isEqualTo(400);
    }

    @Test
    void it_should_return_bad_request_when_link_controller_convert_request_weblink_does_not_have_content_id() throws Exception {
        //given
        LinkConvertRequest linkConvertRequest = LinkConvertRequest.builder()
                .url(WEB_REQUEST_NOT_HAVE_CONTENT_ID_URL)
                .destinationLinkType(LinkConverter.Type.DEEP_LINK)
                .build();
        String requestContent = objectMapper.writeValueAsString(linkConvertRequest);

        //when
        MvcResult mvcResult = mockMvc.perform(post("/link/convert")
                .content(requestContent)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        //then
        assertThat(mvcResult.getResponse().getStatus()).isEqualTo(400);
    }

    @Test
    void it_should_return_bad_request_when_link_controller_convert_request_deeplink_does_not_have_content_id() throws Exception {
        //given
        LinkConvertRequest linkConvertRequest = LinkConvertRequest.builder()
                .url(DEEPLINK_REQUEST_NOT_HAVE_CONTENT_ID_URL)
                .destinationLinkType(LinkConverter.Type.WEB_LINK)
                .build();
        String requestContent = objectMapper.writeValueAsString(linkConvertRequest);

        //when
        MvcResult mvcResult = mockMvc.perform(post("/link/convert")
                .content(requestContent)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        //then
        assertThat(mvcResult.getResponse().getStatus()).isEqualTo(400);
    }
}
