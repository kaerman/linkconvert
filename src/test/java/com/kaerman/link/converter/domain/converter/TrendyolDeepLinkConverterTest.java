package com.kaerman.link.converter.domain.converter;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TrendyolDeepLinkConverterTest {

    private static final TrendyolDeepLinkConverter trendyolDeepLinkConverter = TrendyolDeepLinkConverter.builder().build();

    @Test
    void it_should_convert_deeplink_main_link_when_link_converted() {
        //given
        String mainPageLink = "https://www.trendyol.com";
        //when
        String convertedMainPageLink = trendyolDeepLinkConverter.convert(mainPageLink);
        //then
        assertThat(convertedMainPageLink).isEqualTo("ty://?Page=Home");
    }

    @Test
    void it_should_convert_deeplink_product_link_full_parameter_when_link_converted() {
        //given
        String mainPageLink = "https://www.trendyol.com/casio/saat-p-1925865?boutiqueId=439892&merchantId=105064";
        //when
        String convertedMainPageLink = trendyolDeepLinkConverter.convert(mainPageLink);
        //then
        assertThat(convertedMainPageLink).isEqualTo("ty://?Page=Product&ContentId=1925865&CampaignId=439892&MerchantId=105064");
    }

    @Test
    void it_should_convert_deeplink_product_link_only_contentId_when_link_converted() {
        //given
        String mainPageLink = "https://www.trendyol.com/casio/erkek-kol-saati-p-1925865";
        //when
        String convertedMainPageLink = trendyolDeepLinkConverter.convert(mainPageLink);
        //then
        assertThat(convertedMainPageLink).isEqualTo("ty://?Page=Product&ContentId=1925865");
    }

    @Test
    void it_should_convert_deeplink_product_link_contentId_and_boutiqueId_when_link_converted() {
        //given
        String mainPageLink = "https://www.trendyol.com/casio/erkek-kol-saati-p-1925865?boutiqueId=439892";
        //when
        String convertedMainPageLink = trendyolDeepLinkConverter.convert(mainPageLink);
        //then
        assertThat(convertedMainPageLink).isEqualTo("ty://?Page=Product&ContentId=1925865&CampaignId=439892");
    }

    @Test
    void it_should_convert_deeplink_product_link_contentId_and_merchantId_when_link_converted() {
        //given
        String mainPageLink = "https://www.trendyol.com/casio/erkek-kol-saati-p-1925865?merchantId=105064";
        //when
        String convertedMainPageLink = trendyolDeepLinkConverter.convert(mainPageLink);
        //then
        assertThat(convertedMainPageLink).isEqualTo("ty://?Page=Product&ContentId=1925865&MerchantId=105064");
    }

    @Test
    void it_should_convert_deeplink_search_link_when_link_converted() {
        //given
        String mainPageLink = "https://www.trendyol.com/sr?q=elbise";
        //when
        String convertedMainPageLink = trendyolDeepLinkConverter.convert(mainPageLink);
        //then
        assertThat(convertedMainPageLink).isEqualTo("ty://?Page=Search&Query=elbise");
    }

    @Test
    void it_should_convert_deeplink_search_link_special_characters_when_link_converted() {
        //given
        String mainPageLink = "https://www.trendyol.com/sr?q=%C3%BCt%C3%BC";
        //when
        String convertedMainPageLink = trendyolDeepLinkConverter.convert(mainPageLink);
        //then
        assertThat(convertedMainPageLink).isEqualTo("ty://?Page=Search&Query=%C3%BCt%C3%BC");
    }

    @Test
    void it_should_convert_deeplink_main_link_no_link_match_favorites_when_link_converted() {
        //given
        String mainPageLink = "https://www.trendyol.com/Hesabim/Favoriler";
        //when
        String convertedMainPageLink = trendyolDeepLinkConverter.convert(mainPageLink);
        //then
        assertThat(convertedMainPageLink).isEqualTo("ty://?Page=Home");
    }

    @Test
    void it_should_convert_deeplink_main_link_no_link_match_order_when_link_converted() {
        //given
        String mainPageLink = "https://www.trendyol.com/Hesabim/#/Siparislerim";
        //when
        String convertedMainPageLink = trendyolDeepLinkConverter.convert(mainPageLink);
        //then
        assertThat(convertedMainPageLink).isEqualTo("ty://?Page=Home");
    }

}