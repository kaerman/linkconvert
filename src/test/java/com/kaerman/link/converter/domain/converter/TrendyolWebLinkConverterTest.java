package com.kaerman.link.converter.domain.converter;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TrendyolWebLinkConverterTest {

    private static final TrendyolWebLinkConverter trendyolWebLinkConverter = TrendyolWebLinkConverter.builder().build();

    @Test
    void it_should_convert_deeplink_main_link_when_link_converted() {
        //given
        String mainPageLink = "ty://?Page=Product&ContentId=1925865&CampaignId=439892&MerchantId=105064";
        //when
        String convertedMainPageLink = trendyolWebLinkConverter.convert(mainPageLink);
        //then
        assertThat(convertedMainPageLink).isEqualTo("https://www.trendyol.com/brand/name-p-1925865?boutiqueId=439892&merchantId=105064");
    }

    @Test
    void it_should_convert_deeplink_product_link_full_parameter_when_link_converted() {
        //given
        String mainPageLink = "ty://?Page=Product&ContentId=1925865&CampaignId=439892&MerchantId=105064";
        //when
        String convertedMainPageLink = trendyolWebLinkConverter.convert(mainPageLink);
        //then
        assertThat(convertedMainPageLink).isEqualTo("https://www.trendyol.com/brand/name-p-1925865?boutiqueId=439892&merchantId=105064");
    }

    @Test
    void it_should_convert_deeplink_product_link_only_contentId_when_link_converted() {
        //given
        String mainPageLink = "ty://?Page=Product&ContentId=1925865";
        //when
        String convertedMainPageLink = trendyolWebLinkConverter.convert(mainPageLink);
        //then
        assertThat(convertedMainPageLink).isEqualTo("https://www.trendyol.com/brand/name-p-1925865");
    }

    @Test
    void it_should_convert_deeplink_product_link_contentId_and_boutiqueId_when_link_converted() {
        //given
        String mainPageLink = "ty://?Page=Product&ContentId=1925865&CampaignId=439892";
        //when
        String convertedMainPageLink = trendyolWebLinkConverter.convert(mainPageLink);
        //then
        assertThat(convertedMainPageLink).isEqualTo("https://www.trendyol.com/brand/name-p-1925865?boutiqueId=439892");
    }

    @Test
    void it_should_convert_deeplink_product_link_contentId_and_merchantId_when_link_converted() {
        //given
        String mainPageLink = "ty://?Page=Product&ContentId=1925865&MerchantId=105064";
        //when
        String convertedMainPageLink = trendyolWebLinkConverter.convert(mainPageLink);
        //then
        assertThat(convertedMainPageLink).isEqualTo("https://www.trendyol.com/brand/name-p-1925865?merchantId=105064");
    }

    @Test
    void it_should_convert_deeplink_search_link_when_link_converted() {
        //given
        String mainPageLink = "ty://?Page=Search&Query=elbise";
        //when
        String convertedMainPageLink = trendyolWebLinkConverter.convert(mainPageLink);
        //then
        assertThat(convertedMainPageLink).isEqualTo("https://www.trendyol.com/sr?q=elbise");
    }

    @Test
    void it_should_convert_deeplink_search_link_special_characters_when_link_converted() {
        //given
        String mainPageLink = "ty://?Page=Search&Query=%C3%BCt%C3%BC";
        //when
        String convertedMainPageLink = trendyolWebLinkConverter.convert(mainPageLink);
        //then
        assertThat(convertedMainPageLink).isEqualTo("https://www.trendyol.com/sr?q=%C3%BCt%C3%BC");
    }

    @Test
    void it_should_convert_deeplink_main_link_no_link_match_favorites_when_link_converted() {
        //given
        String mainPageLink = "ty://?Page=Favorites";
        //when
        String convertedMainPageLink = trendyolWebLinkConverter.convert(mainPageLink);
        //then
        assertThat(convertedMainPageLink).isEqualTo("https://www.trendyol.com");
    }

    @Test
    void it_should_convert_deeplink_main_link_no_link_match_order_when_link_converted() {
        //given
        String mainPageLink = "ty://?Page=Orders";
        //when
        String convertedMainPageLink = trendyolWebLinkConverter.convert(mainPageLink);
        //then
        assertThat(convertedMainPageLink).isEqualTo("https://www.trendyol.com");
    }


}