package com.kaerman.link.converter.domain;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class LinkTest {

    @Test
    void it_should_parse_web_full_link_when_link_parsed() {
        //given
        String webLink = "https://www.trendyol.com:8080/casio/saat-p-1925865?boutiqueId=439892&merchantId=105064";
        //when
        Link link = Link.parse(webLink);
        //then
        assertThat(link.getScheme()).isEqualTo("https");
        assertThat(link.getHost()).isEqualTo("www.trendyol.com");
        assertThat(link.getPort()).isEqualTo(8080);
        assertThat(link.getPath()).isEqualTo("casio/saat-p-1925865");
        assertThat(link.getQuery()).isEqualTo("boutiqueId=439892&merchantId=105064");
        assertThat(link.toString()).isEqualTo(webLink);
    }

    @Test
    void it_should_parse_web_link_empty_port_when_link_parsed() {
        //given
        String webLink = "https://www.trendyol.com:/casio/saat-p-1925865?boutiqueId=439892&merchantId=105064";
        //when
        Link link = Link.parse(webLink);
        //then
        assertThat(link.getScheme()).isEqualTo("https");
        assertThat(link.getHost()).isEqualTo("www.trendyol.com");
        assertThat(link.getPath()).isEqualTo("casio/saat-p-1925865");
        assertThat(link.getQuery()).isEqualTo("boutiqueId=439892&merchantId=105064");
    }

    @Test
    void it_should_parse_web_link_without_port_when_link_parsed() {
        //given
        String webLink = "https://www.trendyol.com/casio/saat-p-1925865?boutiqueId=439892&merchantId=105064";
        //when
        Link link = Link.parse(webLink);
        //then
        assertThat(link.getScheme()).isEqualTo("https");
        assertThat(link.getHost()).isEqualTo("www.trendyol.com");
        assertThat(link.getPath()).isEqualTo("casio/saat-p-1925865");
        assertThat(link.getQuery()).isEqualTo("boutiqueId=439892&merchantId=105064");
        assertThat(link.toString()).isEqualTo(webLink);
    }

    @Test
    void it_should_parse_web_link_empty_port_and_query_when_link_parsed() {
        //given
        String webLink = "https://www.trendyol.com:/casio/saat-p-1925865?";
        //when
        Link link = Link.parse(webLink);
        //then
        assertThat(link.getScheme()).isEqualTo("https");
        assertThat(link.getHost()).isEqualTo("www.trendyol.com");
        assertThat(link.getPath()).isEqualTo("casio/saat-p-1925865");
    }

    @Test
    void it_should_parse_web_link_without_port_and_query_when_link_parsed() {
        //given
        String webLink = "https://www.trendyol.com/casio/saat-p-1925865";
        //when
        Link link = Link.parse(webLink);
        //then
        assertThat(link.getScheme()).isEqualTo("https");
        assertThat(link.getHost()).isEqualTo("www.trendyol.com");
        assertThat(link.getPath()).isEqualTo("casio/saat-p-1925865");
        assertThat(link.toString()).isEqualTo(webLink);
    }

    @Test
    void it_should_parse_web_link_empty_port_and_query_and_path_when_link_parsed() {
        //given
        String webLink = "https://www.trendyol.com:/?";
        //when
        Link link = Link.parse(webLink);
        //then
        assertThat(link.getScheme()).isEqualTo("https");
        assertThat(link.getHost()).isEqualTo("www.trendyol.com");
    }


    @Test
    void it_should_parse_web_link_without_port_and_query_and_path_when_link_parsed() {
        //given
        String webLink = "https://www.trendyol.com";
        //when
        Link link = Link.parse(webLink);
        //then
        assertThat(link.getScheme()).isEqualTo("https");
        assertThat(link.getHost()).isEqualTo("www.trendyol.com");
        assertThat(link.toString()).isEqualTo(webLink);
    }

    @Test
    void it_should_parse_deep_link_when_link_parsed() {
        //given
        String deepLink = "ty://?Page=Product&ContentId=1925865&CampaignId=439892&MerchantId=105064";
        //when
        Link link = Link.parse(deepLink);
        //then
        assertThat(link.getScheme()).isEqualTo("ty");
        assertThat(link.getHost()).isBlank();
        assertThat(link.getPath()).isBlank();
        assertThat(link.getQuery()).isEqualTo("Page=Product&ContentId=1925865&CampaignId=439892&MerchantId=105064");
    }

    @Test
    void it_should_parse_deep_link_empy_query_when_link_parsed() {
        //given
        String deepLink = "ty://?";
        //when
        Link link = Link.parse(deepLink);
        //then
        assertThat(link.getScheme()).isEqualTo("ty");
        assertThat(link.getHost()).isBlank();
        assertThat(link.getPath()).isBlank();
        assertThat(link.getQuery()).isBlank();
    }

    @Test
    void it_should_parse_deep_link_without_query_when_link_parsed() {
        //given
        String deepLink = "ty://";
        //when
        Link link = Link.parse(deepLink);
        //then
        assertThat(link.getScheme()).isEqualTo("ty");
        assertThat(link.getHost()).isBlank();
        assertThat(link.getPath()).isBlank();
        assertThat(link.getQuery()).isBlank();
    }

}