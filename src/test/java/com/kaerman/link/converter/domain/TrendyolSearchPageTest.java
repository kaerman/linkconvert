package com.kaerman.link.converter.domain;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TrendyolSearchPageTest {

    @Test
    void it_should_match_web_search_page_when_link_matcher() {
        //given
        Link link = Link.parse("https://www.trendyol.com/sr?q=%C3%BCt%C3%BC");
        //when
        boolean matchResult = TrendyolPageFactory.trendyolPageMap.get(TrendyolPage.TrendyolPageType.SEARCH_PAGE).linkMatcher(link);
        //then
        assertThat(matchResult).isTrue();
    }

    @Test
    void it_should_match_deeplink_search_page_when_link_matcher() {
        //given
        Link link = Link.parse("ty://?Page=Search&Query=%C3%BCt%C3%BC");
        //when
        boolean matchResult = TrendyolPageFactory.trendyolPageMap.get(TrendyolPage.TrendyolPageType.SEARCH_PAGE).linkMatcher(link);
        //then
        assertThat(matchResult).isTrue();
    }

    @Test
    void it_should_not_match_web_search_page_when_link_matcher() {
        //given
        Link link = Link.parse("https://www.trendyol.com/casio/saat-p-1925865?boutiqueId=439892&merchantId=105064");
        //when
        boolean matchResult = TrendyolPageFactory.trendyolPageMap.get(TrendyolPage.TrendyolPageType.SEARCH_PAGE).linkMatcher(link);
        //then
        assertThat(matchResult).isFalse();
    }

    @Test
    void it_should_not_match_deeplink_search_page_when_link_matcher() {
        //given
        Link link = Link.parse("ty://?Page=Product&ContentId=1925865&CampaignId=439892&MerchantId=105064");
        //when
        boolean matchResult = TrendyolPageFactory.trendyolPageMap.get(TrendyolPage.TrendyolPageType.SEARCH_PAGE).linkMatcher(link);
        //then
        assertThat(matchResult).isFalse();
    }

    @Test
    void it_should_create_web_search_page_when_create_page() {
        //given
        Link link = Link.parse("https://www.trendyol.com/sr?q=%C3%BCt%C3%BC");
        //when
        TrendyolSearchPage createdTrendyolSearchPage = (TrendyolSearchPage) TrendyolPageFactory.createTrendyolPage(link);
        //then
        assertThat(createdTrendyolSearchPage.getTrendyolPageType()).isEqualTo(TrendyolPage.TrendyolPageType.SEARCH_PAGE);
        assertThat(createdTrendyolSearchPage.getQuery()).isEqualTo("%C3%BCt%C3%BC");
    }

    @Test
    void it_should_match_deeplink_search_page_when_create_page() {
        //given
        Link link = Link.parse("ty://?Page=Search&Query=%C3%BCt%C3%BC");
        //when
        TrendyolSearchPage createdTrendyolSearchPage = (TrendyolSearchPage) TrendyolPageFactory.createTrendyolPage(link);
        //then
        assertThat(createdTrendyolSearchPage.getTrendyolPageType()).isEqualTo(TrendyolPage.TrendyolPageType.SEARCH_PAGE);
        assertThat(createdTrendyolSearchPage.getQuery()).isEqualTo("%C3%BCt%C3%BC");
    }

    @Test
    void it_should_create_web_search_page_when_query_includes_turkish_character() {
        //given
        Link link = Link.parse("https://www.trendyol.com/sr?q=ütü");
        //when
        TrendyolSearchPage createdTrendyolSearchPage = (TrendyolSearchPage) TrendyolPageFactory.createTrendyolPage(link);
        //then
        assertThat(createdTrendyolSearchPage.getTrendyolPageType()).isEqualTo(TrendyolPage.TrendyolPageType.SEARCH_PAGE);
        assertThat(createdTrendyolSearchPage.getQuery()).isEqualTo("%C3%BCt%C3%BC");
    }

    @Test
    void it_should_match_deeplink_search_page_when_query_includes_turkish_character() {
        //given
        Link link = Link.parse("ty://?Page=Search&Query=ütü");
        //when
        TrendyolSearchPage createdTrendyolSearchPage = (TrendyolSearchPage) TrendyolPageFactory.createTrendyolPage(link);
        //then
        assertThat(createdTrendyolSearchPage.getTrendyolPageType()).isEqualTo(TrendyolPage.TrendyolPageType.SEARCH_PAGE);
    }

}