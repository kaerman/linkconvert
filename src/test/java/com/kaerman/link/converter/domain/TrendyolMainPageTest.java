package com.kaerman.link.converter.domain;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TrendyolMainPageTest {

    @Test
    void it_should_match_web_main_page_when_link_matcher() {
        //given
        Link link = Link.parse("https://www.trendyol.com");
        //when
        boolean matchResult = TrendyolPageFactory.trendyolPageMap.get(TrendyolPage.TrendyolPageType.MAIN_PAGE).linkMatcher(link);
        //then
        assertThat(matchResult).isTrue();
    }

    @Test
    void it_should_match_deeplink_main_page_when_link_matcher() {
        //given
        Link link = Link.parse("ty://?Page=Home");
        //when
        boolean matchResult = TrendyolPageFactory.trendyolPageMap.get(TrendyolPage.TrendyolPageType.MAIN_PAGE).linkMatcher(link);
        //then
        assertThat(matchResult).isTrue();
    }

    @Test
    void it_should_not_match_web_main_page_when_link_matcher() {
        //given
        Link link = Link.parse("https://www.trendyol.com/sr?q=%C3%BCt%C3%BC");
        //when
        boolean matchResult = TrendyolPageFactory.trendyolPageMap.get(TrendyolPage.TrendyolPageType.MAIN_PAGE).linkMatcher(link);
        //then
        assertThat(matchResult).isFalse();
    }

    @Test
    void it_should_not_match_deeplink_main_page_when_link_matcher() {
        //given
        Link link = Link.parse("ty://?Page=Search&Query=%C3%BCt%C3%BC");
        //when
        boolean matchResult = TrendyolPageFactory.trendyolPageMap.get(TrendyolPage.TrendyolPageType.MAIN_PAGE).linkMatcher(link);
        //then
        assertThat(matchResult).isFalse();
    }

    @Test
    void it_should_create_web_main_page_when_create_page() {
        //given
        Link link = Link.parse("https://www.trendyol.com");
        //when
        TrendyolMainPage createdTrendyolMainPage = (TrendyolMainPage) TrendyolPageFactory.trendyolPageMap.get(TrendyolPage.TrendyolPageType.MAIN_PAGE).createPage(link);
        //then
        assertThat(createdTrendyolMainPage.getTrendyolPageType()).isEqualTo(TrendyolPage.TrendyolPageType.MAIN_PAGE);
    }

    @Test
    void it_should_create_deeplink_main_page_when_create_page() {
        //given
        Link link = Link.parse("ty://?Page=Home");
        //when
        TrendyolMainPage createdTrendyolMainPage = (TrendyolMainPage) TrendyolPageFactory.trendyolPageMap.get(TrendyolPage.TrendyolPageType.MAIN_PAGE).createPage(link);
        //then
        assertThat(createdTrendyolMainPage.getTrendyolPageType()).isEqualTo(TrendyolPage.TrendyolPageType.MAIN_PAGE);
    }
}