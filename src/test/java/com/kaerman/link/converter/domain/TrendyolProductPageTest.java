package com.kaerman.link.converter.domain;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TrendyolProductPageTest {

    @Test
    void it_should_match_web_product_page_when_link_matcher() {
        //given
        Link link = Link.parse("https://www.trendyol.com/casio/saat-p-1925865?boutiqueId=439892&merchantId=105064");
        //when
        boolean matchResult = TrendyolPageFactory.trendyolPageMap.get(TrendyolPage.TrendyolPageType.PRODUCT_PAGE).linkMatcher(link);
        //then
        assertThat(matchResult).isTrue();
    }

    @Test
    void it_should_match_deeplink_product_page_when_link_matcher() {
        //given
        Link link = Link.parse("ty://?Page=Product&ContentId=1925865&CampaignId=439892&MerchantId=105064");
        //when
        boolean matchResult = TrendyolPageFactory.trendyolPageMap.get(TrendyolPage.TrendyolPageType.PRODUCT_PAGE).linkMatcher(link);
        //then
        assertThat(matchResult).isTrue();
    }

    @Test
    void it_should_not_match_web_product_page_when_link_matcher() {
        //given
        Link link = Link.parse("https://www.trendyol.com/sr?q=%C3%BCt%C3%BC");
        //when
        boolean matchResult = TrendyolPageFactory.trendyolPageMap.get(TrendyolPage.TrendyolPageType.PRODUCT_PAGE).linkMatcher(link);
        //then
        assertThat(matchResult).isFalse();
    }

    @Test
    void it_should_not_match_deeplink_product_page_when_link_matcher() {
        //given
        Link link = Link.parse("ty://?Page=Search&Query=%C3%BCt%C3%BC");
        //when
        boolean matchResult = TrendyolPageFactory.trendyolPageMap.get(TrendyolPage.TrendyolPageType.PRODUCT_PAGE).linkMatcher(link);
        //then
        assertThat(matchResult).isFalse();
    }

    @Test
    void it_should_create_web_product_page_when_create_page() {
        //given
        Link link = Link.parse("https://www.trendyol.com/casio/saat-p-1925865?boutiqueId=439892&merchantId=105064");
        //when
        TrendyolProductPage createdTrendyolProductPage = (TrendyolProductPage) TrendyolPageFactory.createTrendyolPage(link);
        //then
        assertThat(createdTrendyolProductPage.getTrendyolPageType()).isEqualTo(TrendyolPage.TrendyolPageType.PRODUCT_PAGE);
        assertThat(createdTrendyolProductPage.getBrandName()).isEqualTo("casio");
        assertThat(createdTrendyolProductPage.getProductName()).isEqualTo("saat");
        assertThat(createdTrendyolProductPage.getContentId()).isEqualTo("1925865");
        assertThat(createdTrendyolProductPage.getBoutiqueId()).isEqualTo("439892");
        assertThat(createdTrendyolProductPage.getMerchantId()).isEqualTo("105064");
    }

    @Test
    void it_should_create_deeplink_product_page_when_create_page() {
        //given
        Link link = Link.parse("ty://?Page=Product&ContentId=1925865&CampaignId=439892&MerchantId=105064");
        //when
        TrendyolProductPage createdTrendyolProductPage = (TrendyolProductPage) TrendyolPageFactory.createTrendyolPage(link);
        //then
        assertThat(createdTrendyolProductPage.getTrendyolPageType()).isEqualTo(TrendyolPage.TrendyolPageType.PRODUCT_PAGE);
        assertThat(createdTrendyolProductPage.getContentId()).isEqualTo("1925865");
        assertThat(createdTrendyolProductPage.getBoutiqueId()).isEqualTo("439892");
        assertThat(createdTrendyolProductPage.getMerchantId()).isEqualTo("105064");
    }

}