package com.kaerman.link.converter.domain;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TrendyolPageFactoryTest {

    @Test
    void it_should_return_main_page_when_web_link_matcher() {
        //given
        Link link = Link.parse("https://www.trendyol.com");
        //when
        TrendyolMainPage trendyolMainPage = (TrendyolMainPage) TrendyolPageFactory.createTrendyolPage(link);
        //then
        assertThat(trendyolMainPage.getTrendyolPageType()).isEqualTo(TrendyolPage.TrendyolPageType.MAIN_PAGE);
    }

    @Test
    void it_should_return_main_page_when_deep_link_matcher() {
        //given
        Link link = Link.parse("ty://?Page=Home");
        //when
        TrendyolMainPage trendyolMainPage = (TrendyolMainPage) TrendyolPageFactory.createTrendyolPage(link);
        //then
        assertThat(trendyolMainPage.getTrendyolPageType()).isEqualTo(TrendyolPage.TrendyolPageType.MAIN_PAGE);
    }

    @Test
    void it_should_return_product_page_when_web_link_matcher() {
        //given
        Link link = Link.parse("https://www.trendyol.com/casio/saat-p-1925865?boutiqueId=439892&merchantId=105064");
        //when
        TrendyolProductPage trendyolProductPage = (TrendyolProductPage) TrendyolPageFactory.createTrendyolPage(link);
        //then
        assertThat(trendyolProductPage.getTrendyolPageType()).isEqualTo(TrendyolPage.TrendyolPageType.PRODUCT_PAGE);
    }

    @Test
    void it_should_return_search_page_when_web_link_matcher() {
        //given
        Link link = Link.parse("https://www.trendyol.com/sr?q=%C3%BCt%C3%BC");
        //when
        TrendyolSearchPage trendyolSearchPage = (TrendyolSearchPage) TrendyolPageFactory.createTrendyolPage(link);
        //then
        assertThat(trendyolSearchPage.getTrendyolPageType()).isEqualTo(TrendyolPage.TrendyolPageType.SEARCH_PAGE);
    }

    @Test
    void it_should_return_product_page_when_deep_link_matcher() {
        //given
        Link link = Link.parse("ty://?Page=Product&ContentId=1925865&CampaignId=439892&MerchantId=105064");
        //when
        TrendyolProductPage trendyolProductPage = (TrendyolProductPage) TrendyolPageFactory.createTrendyolPage(link);
        //then
        assertThat(trendyolProductPage.getTrendyolPageType()).isEqualTo(TrendyolPage.TrendyolPageType.PRODUCT_PAGE);
    }

    @Test
    void it_should_return_search_page_when_deep_link_matcher() {
        //given
        Link link = Link.parse("ty://?Page=Search&Query=%C3%BCt%C3%BC");
        //when
        TrendyolSearchPage trendyolSearchPage = (TrendyolSearchPage) TrendyolPageFactory.createTrendyolPage(link);
        //then
        assertThat(trendyolSearchPage.getTrendyolPageType()).isEqualTo(TrendyolPage.TrendyolPageType.SEARCH_PAGE);
    }


}