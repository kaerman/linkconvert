package com.kaerman.link.converter.domain.converter;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class LinkConverterFactoryTest {

    public static LinkConverterFactory linkConverterFactory;

    @BeforeAll
    static void init() {
        Set<LinkConverter> linkConverterSet = new HashSet<>();
        linkConverterSet.add(TrendyolDeepLinkConverter.builder().build());
        linkConverterSet.add(TrendyolWebLinkConverter.builder().build());
        linkConverterFactory = new LinkConverterFactory(linkConverterSet);
    }

    @Test
    void it_should_return_deep_link_converter_when_get_link_converter() {
        //given
        LinkConverter.Type type = LinkConverter.Type.DEEP_LINK;
        //when
        TrendyolDeepLinkConverter trendyolDeepLinkConverter = (TrendyolDeepLinkConverter) linkConverterFactory.getLinkConverter(type);
        //then
        assertThat(trendyolDeepLinkConverter.getType()).isEqualTo(LinkConverter.Type.DEEP_LINK);
    }

    @Test
    void it_should_return_web_link_converter_when_get_link_converter() {
        //given
        LinkConverter.Type type = LinkConverter.Type.WEB_LINK;
        //when
        TrendyolWebLinkConverter trendyolWebLinkConverter = (TrendyolWebLinkConverter) linkConverterFactory.getLinkConverter(type);
        //then
        assertThat(trendyolWebLinkConverter.getType()).isEqualTo(LinkConverter.Type.WEB_LINK);
    }
}