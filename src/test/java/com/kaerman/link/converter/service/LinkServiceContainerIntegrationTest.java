package com.kaerman.link.converter.service;

import com.kaerman.link.converter.domain.converter.LinkConverter;
import com.kaerman.link.converter.domain.document.LinkConvertMessage;
import com.kaerman.link.converter.dto.LinkConvertRequest;
import com.kaerman.link.converter.dto.LinkConvertResponse;
import com.kaerman.link.converter.repository.LinkRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.elasticsearch.ElasticsearchContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@Testcontainers
@ContextConfiguration(initializers = {LinkServiceContainerIntegrationTest.Initializer.class})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LinkServiceContainerIntegrationTest {

    private static final String WEB_REQUEST_URL = "https://www.trendyol.com/casio/saat-p-1925865?boutiqueId=439892&merchantId=105064";
    private static final String DEEPLINK_RESPONSE_URL = "ty://?Page=Product&ContentId=1925865&CampaignId=439892&MerchantId=105064";
    private static final String WEB_RESPONSE_URL = "https://www.trendyol.com/brand/name-p-1925865?boutiqueId=439892&merchantId=105064";
    private static final String DEEPLINK_REQUEST_URL = "ty://?Page=Product&ContentId=1925865&CampaignId=439892&MerchantId=105064";

    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "elasticSearch.hostAndPort=" + elasticsearchContainer.getHttpHostAddress()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }

    @Container
    public static ElasticsearchContainer elasticsearchContainer = new ElasticsearchContainer("docker.elastic.co/elasticsearch/elasticsearch:7.12.0")
            .withExposedPorts(9200, 9300);

    @Autowired
    private LinkService linkService;

    @Autowired
    LinkRepository linkRepository;

    @Test
    public void it_should_save_linkConvertMessage_when_link_service_save_converted_to_deeplink() {
        //given
        LinkConvertRequest linkConvertRequest = LinkConvertRequest.builder()
                .url(WEB_REQUEST_URL)
                .destinationLinkType(LinkConverter.Type.DEEP_LINK)
                .build();
        LinkConvertResponse linkConvertResponse = LinkConvertResponse.builder()
                .url(DEEPLINK_RESPONSE_URL)
                .linkType(LinkConverter.Type.DEEP_LINK)
                .build();
        LinkConvertMessage linkConvertMessage = LinkConvertMessage.builder()
                .requestMessage(linkConvertRequest)
                .responseMessage(linkConvertResponse)
                .timestamp(new Date())
                .build();

        //when
        LinkConvertMessage savedLinkConvertMessage = linkService.saveLinkConvertMessage(linkConvertMessage);

        //then
        assertThat(savedLinkConvertMessage.getId()).isNotBlank();
        Optional<LinkConvertMessage> convertMessage = linkRepository.findById(savedLinkConvertMessage.getId());
        assertThat(convertMessage.isPresent()).isTrue();
        assertThat(convertMessage.get().getId()).isEqualTo(savedLinkConvertMessage.getId());
    }

    @Test
    public void it_should_convert_to_deeplink_when_link_service_convert_link_converted_to_deeplink() {
        //given
        LinkConvertRequest linkConvertRequest = LinkConvertRequest.builder()
                .url(WEB_REQUEST_URL)
                .destinationLinkType(LinkConverter.Type.DEEP_LINK)
                .build();

        //when
        String convertedLink = linkService.convertLink(linkConvertRequest);

        //then
        assertThat(convertedLink).isNotBlank();
        assertThat(convertedLink).isEqualTo(DEEPLINK_RESPONSE_URL);
    }

    @Test
    public void it_should_save_linkConvertMessage_when_link_service_save_converted_to_web() {
        //given
        LinkConvertRequest linkConvertRequest = LinkConvertRequest.builder()
                .url(DEEPLINK_REQUEST_URL)
                .destinationLinkType(LinkConverter.Type.WEB_LINK)
                .build();
        LinkConvertResponse linkConvertResponse = LinkConvertResponse.builder()
                .url(WEB_RESPONSE_URL)
                .linkType(LinkConverter.Type.WEB_LINK)
                .build();
        LinkConvertMessage linkConvertMessage = LinkConvertMessage.builder()
                .requestMessage(linkConvertRequest)
                .responseMessage(linkConvertResponse)
                .timestamp(new Date())
                .build();

        //when
        LinkConvertMessage savedLinkConvertMessage = linkService.saveLinkConvertMessage(linkConvertMessage);

        //then
        assertThat(savedLinkConvertMessage.getId()).isNotBlank();
        Optional<LinkConvertMessage> convertMessage = linkRepository.findById(savedLinkConvertMessage.getId());
        assertThat(convertMessage.isPresent()).isTrue();
        assertThat(convertMessage.get().getId()).isEqualTo(savedLinkConvertMessage.getId());
    }

    @Test
    public void it_should_convert_to_weblink_when_link_service_convert_link_converted_to_web() {
        //given
        LinkConvertRequest linkConvertRequest = LinkConvertRequest.builder()
                .url(DEEPLINK_REQUEST_URL)
                .destinationLinkType(LinkConverter.Type.WEB_LINK)
                .build();

        //when
        String convertedLink = linkService.convertLink(linkConvertRequest);

        //then
        assertThat(convertedLink).isNotBlank();
        assertThat(convertedLink).isEqualTo(WEB_RESPONSE_URL);
    }
}
