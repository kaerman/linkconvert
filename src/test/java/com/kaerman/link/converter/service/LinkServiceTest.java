package com.kaerman.link.converter.service;

import com.kaerman.link.converter.domain.converter.LinkConverter;
import com.kaerman.link.converter.domain.document.LinkConvertMessage;
import com.kaerman.link.converter.dto.LinkConvertRequest;
import com.kaerman.link.converter.dto.LinkConvertResponse;
import com.kaerman.link.converter.repository.LinkRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.*;

@ExtendWith(MockitoExtension.class)
class LinkServiceTest {

    private static final String WEB_REQUEST_URL = "https://www.trendyol.com/casio/saat-p-1925865?boutiqueId=439892&merchantId=105064";
    private static final String DEEPLINK_RESPONSE_URL = "ty://?Page=Product&ContentId=1925865&CampaignId=439892&MerchantId=105064";

    private static final String WEB_RESPONSE_URL = "https://www.trendyol.com/casio/saat-p-1925865?boutiqueId=439892&merchantId=105064";
    private static final String DEEPLINK_REQUEST_URL = "ty://?Page=Product&ContentId=1925865&CampaignId=439892&MerchantId=105064";

    @Mock
    private LinkRepository linkRepository;

    @InjectMocks
    private LinkService linkService;

    @Captor
    ArgumentCaptor<LinkConvertMessage> linkConvertMessageArgumentCaptor;

    @Test
    public void it_should_save_linkConvertMessage_when_link_service_save_with_web() {
        //given
        LinkConvertRequest linkConvertRequest = LinkConvertRequest.builder()
                .url(WEB_REQUEST_URL)
                .destinationLinkType(LinkConverter.Type.DEEP_LINK)
                .build();
        LinkConvertResponse linkConvertResponse = LinkConvertResponse.builder()
                .url(DEEPLINK_RESPONSE_URL)
                .linkType(LinkConverter.Type.DEEP_LINK)
                .build();
        LinkConvertMessage linkConvertMessage = LinkConvertMessage.builder()
                .id(UUID.randomUUID().toString())
                .requestMessage(linkConvertRequest)
                .responseMessage(linkConvertResponse)
                .timestamp(new Date())
                .build();
        given(linkRepository.save(linkConvertMessage)).willReturn(linkConvertMessage);

        //when
        LinkConvertMessage savedLinkConvertMessage = linkService.saveLinkConvertMessage(linkConvertMessage);

        //then
        then(linkRepository).should().save(linkConvertMessageArgumentCaptor.capture());
        List<LinkConvertMessage> linkConvertMessageArgumentCaptorAllValues = linkConvertMessageArgumentCaptor.getAllValues();
        Optional<LinkConvertMessage> argumentCaptorLinkConvertMessage = linkConvertMessageArgumentCaptorAllValues.stream().findFirst();
        assertThat(argumentCaptorLinkConvertMessage.isPresent()).isTrue();
        assertThat(linkConvertMessage.getId()).isEqualTo(savedLinkConvertMessage.getId());
        assertThat(argumentCaptorLinkConvertMessage.get().getRequestMessage().getUrl()).isEqualTo(WEB_REQUEST_URL);
        assertThat(argumentCaptorLinkConvertMessage.get().getResponseMessage().getUrl()).isEqualTo(DEEPLINK_RESPONSE_URL);
    }

    @Test
    public void it_should_save_linkConvertMessage_when_link_service_save_with_deeplink() {
        //given
        LinkConvertRequest linkConvertRequest = LinkConvertRequest.builder()
                .url(DEEPLINK_REQUEST_URL)
                .destinationLinkType(LinkConverter.Type.WEB_LINK)
                .build();
        LinkConvertResponse linkConvertResponse = LinkConvertResponse.builder()
                .url(WEB_RESPONSE_URL)
                .linkType(LinkConverter.Type.WEB_LINK)
                .build();
        LinkConvertMessage linkConvertMessage = LinkConvertMessage.builder()
                .id(UUID.randomUUID().toString())
                .requestMessage(linkConvertRequest)
                .responseMessage(linkConvertResponse)
                .timestamp(new Date())
                .build();
        given(linkRepository.save(linkConvertMessage)).willReturn(linkConvertMessage);

        //when
        LinkConvertMessage savedLinkConvertMessage = linkService.saveLinkConvertMessage(linkConvertMessage);

        //then
        then(linkRepository).should().save(linkConvertMessageArgumentCaptor.capture());
        List<LinkConvertMessage> linkConvertMessageArgumentCaptorAllValues = linkConvertMessageArgumentCaptor.getAllValues();
        Optional<LinkConvertMessage> argumentCaptorLinkConvertMessage = linkConvertMessageArgumentCaptorAllValues.stream().findFirst();
        assertThat(argumentCaptorLinkConvertMessage.isPresent()).isTrue();
        assertThat(linkConvertMessage.getId()).isEqualTo(savedLinkConvertMessage.getId());
        assertThat(argumentCaptorLinkConvertMessage.get().getRequestMessage().getUrl()).isEqualTo(DEEPLINK_REQUEST_URL);
        assertThat(argumentCaptorLinkConvertMessage.get().getResponseMessage().getUrl()).isEqualTo(WEB_RESPONSE_URL);
    }
}