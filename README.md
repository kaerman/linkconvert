# LinkConvert Backend Application

Convert Trendyol.com links between mobile and
web applications.

Basic link schema for LinkConvert backend application:
scheme:[//host[:port]][/]path[?query]

A quick example for URL and deeplink:
Web URL: https://www.trendyol.com/butik/liste/erkek
Equivalent deeplink: ty://?Page=Home&SectionId=2

### Prerequisites

Java JDK 11 or above and Maven

### Installing
Linux\
export JAVA_HOME="/opt/java/jdk-11.0.9"

Compile -> ./mvnw compile\
Package -> ./mvnw package\
Install -> ./mvnw install

### Running the tests

Test   ->  ./mvnw test

### Running Link Convert
Standalone LinkConvert run in target directory -> java -jar link.converter.jar -DelasticSearch.hostAndPort=localhost:9200

LinkConvert, Elastic and Kibana docker build and run -> docker-compose up

### Swagger for Link Convert
Swagger link -> http://localhost:8080/swagger-ui-link-convert.html
