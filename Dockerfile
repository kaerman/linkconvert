# Alpine Linux with OpenJDK JRE
FROM openjdk:11
# copy jar
COPY target/link.converter-0.0.1-SNAPSHOT.jar /app.jar
# runs application
CMD ["java", "-jar", "/app.jar"]
